﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public GameObject playerBox;
    public GameObject boxTarget;
    Vector3 boxOrigin;
    Vector3 boxPos;
    public float speed = 5;

    // Start is called before the first frame update
    void Start()
    {
        boxOrigin = playerBox.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //Debug.Log("space");
            playerBox.transform.position = Vector3.MoveTowards(boxOrigin, boxTarget.transform.position, 1f);
        }
        else
        {
            playerBox.transform.position = Vector3.MoveTowards(transform.position, boxOrigin, 1f);

        }
    }
}
