﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;
using System;
using UnityEngine.UI;

public class BattleMechanic : MonoBehaviour
{
    public int combo;
    bool checking = false;
    bool hit;
    public Text comboCounter;
    public float checkTime;
    public GameObject ComboTile;
    public GameObject ComboText;
    public Vector3 hitPunch;
    private GameObject HitBG;
    public Vector3 missPunchR;
    public Vector3 missPunchP;

    // Start is called before the first frame update
    private void Awake()
    {
        Koreographer.Instance.RegisterForEvents("PreBeat", FireEventDebugLog);
        iTween.Init(gameObject);
    }

    void FireEventDebugLog(KoreographyEvent koreoEvent)
    {
        Debug.Log("Koreo Event Fired");
        StartCoroutine("BeatHit");
        checking = true;
    }

    // Update is called once per frame
    void Update()
    {
        PlayerCheck();
        comboCounter.text = ("" + combo);
    }

    
    private void PlayerCheck()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (checking)
            {
                hit = true;
                Checker();
                comboCounter.text = ("" + combo);
                iTween.PunchScale(ComboTile, hitPunch, .5f);
                iTween.PunchScale(ComboText, hitPunch, .5f);
                iTween.PunchRotation(ComboText, missPunchR, .5f);
                iTween.PunchScale(HitBG, -hitPunch / 2, .5f);

            }
            else if (!checking)
            {
                hit = false;
                combo = 0;
                Checker();
                comboCounter.text = ("" + combo);
                Debug.Log("combo: " + combo);
                iTween.PunchScale(ComboTile, missPunchP, .5f);
            }
        }
    }

    private void Checker()
    {
        if (hit)
        {
            combo++;
            comboCounter.text = ("" + combo);
        }
        else if (checking && !hit)
        {
            combo = 0;
            Debug.Log("combo: " + combo);
        }

    }
    IEnumerator BeatHit()
    {
        Debug.Log("checking");
        comboCounter.text = ("" + combo);
        yield return new WaitForSeconds(checkTime);
        checking = false;
        Debug.Log("NoLongerchecking");
        Debug.Log("combo: " + combo);
        StopCoroutine("BeatHit");
    }
}
