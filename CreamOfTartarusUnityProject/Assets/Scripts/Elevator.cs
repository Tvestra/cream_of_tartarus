﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour {

    private bool onElevator = false;
    private CameraFollow camScript;
    private playerControllerScript playerMove;
    public GameObject elevatorCanvas;
    public GameObject playerHolder;

    public GameObject level1Spawn;
    public GameObject level2Spawn;

    // Use this for initialization
    void Start () {
        // camScript = GetComponent<CameraFollow>();
        camScript = playerHolder.GetComponentInChildren<CameraFollow>();
        playerMove = playerHolder.GetComponentInChildren<playerControllerScript>();
	}
	
	// Update is called once per frame
	void Update () {
        if (onElevator && Input.GetKeyDown(KeyCode.UpArrow ) && !elevatorCanvas.activeInHierarchy)
        {
            Debug.Log("Going Up!");
            elevatorCanvas.SetActive(true);
            playerMove.canMove = false;
        }
        else if (onElevator && Input.GetKeyDown(KeyCode.UpArrow) && elevatorCanvas.activeInHierarchy)
        {
            elevatorCanvas.SetActive(false);
            playerMove.canMove = true;
        }
	}

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("At the elevator");
            onElevator = true;
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("No longer at the elevator");
            onElevator = false;
        }
    }

    public void SelectLevel01()
    {
        elevatorCanvas.SetActive(false);
        playerHolder.transform.position = level1Spawn.transform.position;
        camScript.level = 1;
        playerMove.canMove = true;
    }

    public void SelectLevel02()
    {
        elevatorCanvas.SetActive(false);
        playerHolder.transform.position = level2Spawn.transform.position;
        camScript.level = 2;
        playerMove.canMove = true;
    }
}
