﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CameraFollow : MonoBehaviour
{
	public Transform target;            // The position that that camera will be following.
	public float smoothing = 5f;        // The speed with which the camera will be following.
	
	Vector3 offset;                     // The initial offset from the target.

	float lowY;							// The lowest point the camera will travel.

    public int level = 1;
    public GameObject levelClampL;          // The leftmost that the camera will travel.
    public GameObject levelClampR;          // The Rightmost that the camera will travel.
    public GameObject levelYVal;


    [Header("Level One Clamps")]
    public GameObject level1YVal;
    public GameObject level1LClamp;
    public GameObject level1RClamp;


    [Header("Level Two Clamps")]
    public GameObject level2YVal;
    public GameObject level2LClamp;
    public GameObject level2RClamp;


    public List<GameObject> LevelYVals;
    

	void Start ()
	{
        LevelYVals = new List<GameObject>();
		// Calculate the initial offset.
		offset = transform.position - target.position;

		//calculate the starting Y position
		lowY = levelYVal.transform.position.y;

	}
	
	void FixedUpdate ()
	{
		// Create a postion the camera is aiming for based on the offset from the target.
		Vector3 targetCamPos = target.position + offset;

		// Smoothly interpolate between the camera's current position and it's target position.
		transform.position = Vector3.Lerp (transform.position, targetCamPos, smoothing * Time.deltaTime);
		if (transform.position.y < lowY)  transform.position = new Vector3 (transform.position.x, lowY, transform.position.z);

        Vector3 clamper = transform.position;
        clamper.x = Mathf.Clamp(transform.position.x, levelClampL.transform.position.x,levelClampR.transform.position.x);
        transform.position = clamper;
	}

    public void Update()
    {
        Level();
    }

    public void Level()
    {
        switch (level)
        {
            case 1:
                level = 1;
                levelYVal = level1YVal;
                levelClampL = level1LClamp;
                levelClampR = level1RClamp;
                break;

            case 2:
                level = 2;
                levelYVal = level2YVal;
                levelClampL = level2LClamp;
                levelClampR = level2RClamp;
                break;

            default:
                print("Please Select a level");
            break;
        }
    }
}
