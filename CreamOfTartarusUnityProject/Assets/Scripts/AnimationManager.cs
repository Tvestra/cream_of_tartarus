﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;

public class AnimationManager : MonoBehaviour
{
    private int beats;
    public int fenIndex = 0;
    public GameObject[] fennelAnims;
    public Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        fennelAnims[fenIndex].SetActive(true);
        anim = fennelAnims[fenIndex].GetComponent<Animator>();
        anim.Play(fennelAnims[fenIndex].name);
        beats = 0;
        Koreographer.Instance.RegisterForEvents("PreBeat", BeatCounter);
    }

    void BeatCounter(KoreographyEvent koreoEvent)
    {
        beats++;
        Debug.Log(beats);
        if (beats == 8)
        {
            AnimationSwitch();
        }
    }

    void AnimationSwitch()
    {
        Debug.Log("switch animations");
        fennelAnims[fenIndex].SetActive(false);
        fenIndex++;
        fennelAnims[fenIndex].SetActive(true);
        anim.Play(fennelAnims[fenIndex].name);
    }
}
