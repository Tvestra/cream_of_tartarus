﻿using System.Collections;
using System.Collections.Generic;
using SonicBloom.Koreo;
using UnityEngine;

public class VisualJuice : MonoBehaviour
{
    public Vector3 punch;

    public GameObject ComboTile;
    public GameObject ComboText;
    private AnimationManager animSprites;
    public GameObject curAnim;

    // Start is called before the first frame update
    void Start()
    {
        Koreographer.Instance.RegisterForEvents("OnBeat", FireEventDebugLog);
        animSprites = GetComponent<AnimationManager>();

    }

    private void Update()
    {
       curAnim =  animSprites.fennelAnims[animSprites.fenIndex];
    }

    void FireEventDebugLog(KoreographyEvent koreoEvent)
    {
        iTween.PunchScale(ComboTile, punch, .5f);
        iTween.PunchScale(ComboText, punch, .5f);
        iTween.PunchScale(curAnim, -punch / 2, .5f);
    }
}
